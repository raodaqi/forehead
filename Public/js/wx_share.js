
	 wx.ready(function () {
	 	var title = WX_SHARE_TITLE ? WX_SHARE_TITLE:'用这个查「教务处四六级」成绩太方便了！';
	 	var link = WX_SHARE_LINK ? WX_SHARE_LINK :'http://'+DOMAIN+'/index.php?s=Mobile/Index/appshare';//链接按代换
	 	var imgUrl　=　WX_SHARE_IMGURL ? WX_SHARE_IMGURL: 'http://'+DOMAIN+'/Public/Mobile/img/wechat_share.png';//链接按代换
	 	var desc =　WX_SHARE_DESC ? 　WX_SHARE_DESC:'我正在使用「微查分」查询教务处，四六级等考试成绩，非常好用的查分工具哦！';

	    //分享到朋友圈
		wx.onMenuShareTimeline({
			title: title,
			link: link,
			imgUrl: imgUrl,
			// success: function (res) {
			// 	alert('分享成功');
			// },
			// cancel: function (res) {
			// 	alert('取消成功');
			// },
		});

		//分享给朋友
		wx.onMenuShareAppMessage({
			title: title,
			link: link,
			imgUrl: imgUrl,
			desc: desc,
			// success: function (res) {
		 //        alert('分享成功');
			// },
			// cancel: function (res) {
			// 	alert('取消成功');
			// },
		});

		//分享到qq好友
		wx.onMenuShareQQ({
		   title: title,
			link: link,
			imgUrl: imgUrl,
			desc: desc,
		 //    success: function (res) {
		 //        alert('分享成功');
			// },
			// cancel: function (res) {
			// 	alert('取消成功');
			// },
		});

		//分享到腾讯微博
		wx.onMenuShareWeibo({
		    title: title,
			link: link,
			imgUrl: imgUrl,
			desc: desc,
		 //    success: function (res) {
		 //        alert('分享成功');
			// },
			// cancel: function (res) {
			// 	alert('取消成功');
			// },
		});
		wx.hideAllNonBaseMenuItem();
		wx.showMenuItems({
		    menuList: [
		    	"menuItem:addContact",
		    	"menuItem:share:timeline",
		    	"menuItem:share:appMessage",
		    	"menuItem:share:qq",
		    	"menuItem:share:weiboApp",
		    	"menuItem:refresh",
		    	"menuItem:share:brand",
		    ] // 要显示的菜单项，所有menu项见附录3
		});
	});