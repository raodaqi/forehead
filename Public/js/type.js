$(document).ready(function() {

                $('#yl').on('click', function() {
                    $('#input20').val('yulan');
                });
                $('#tj').on('click', function() {
                    $('#input20').val('');
                });

                // 验证邮箱
                $('#check_email').on('click', function () {

                    var email = $('#select20').val();

                    if (! checkEmail(email)) {
                        return false;
                    }

                    $('#email_for_check').html(email);

                    $.mbox.confirm('#check_email_box', {
                        "boxClass" : 'mbox-2',
                        "icon": null,
                        "showTitle": true,
                        "title": '<img src="' + IMG_DIR + '/logo-3.png" /> 验证邮箱',
                        "okBtnTxt" : '验 证',
                        "contentType" : 'selector',
                        "submitUrl" : "/index.php?s=/addon/Resume/Publish/sendEmailVcode",
                        "submitData" : {"email": email},
                        "success": function (resp, box) {
                            $.mbox.alert(resp.msg, {
                                "icon":'happy',
                                'onok': function (box) {
                                    box.close();
                                    refreshValidateEmailStatus(email);
                                }
                            });
                        }
                    });
                });

                $('.btn-success,.btn-view,#tj').on('click', function() {
                    $('#input7').val($("#select7").val());
                    // $('#input16').val($("#select16").val());
                    $('#input17').val($("#select17").val());
                    $('.email').val($("#select20").val());
                    var a = false;
                    var b = false;
                    var c = false;
                    var d = false;
                    var e = false;
                    var f = false;
                    //没有输入职位名称
                    if ($('#select7').val() == '请输入职位名称' || $('#select7').val() == '')
                    {
                        if ($('#select7').val() == '')
                        {
                            $('#select7').attr('placeholder', '请输入职位名称');
                        }
                        $('#select7').addClass('animation');
                        $('#select7').addClass('animation::-webkit-input-placeholder');
                        $('#select7').focus();
                    }
                    else
                    {
                        a = true;
                    }
                    if ($('#select16').val() == '请输入公司名称' || $('#select16').val() == '')
                    {
                        if ($('#select16').val() == '')
                        {
                            $('#select16').attr('placeholder', '请输入公司名称');
                        }
                        $('#select16').addClass('animation');
                        $('#select16').addClass('animation::-webkit-input-placeholder');
                        $('#select16').focus();

                    } else
                    {
                        b = true;
                    }
                    if ($('#select17').val() == '')
                    {
                        if ($('#select17').val() == '')
                        {
                            $('#select17').attr('placeholder', '请输CEO/管理团队介绍');
                        }
                        $('#select17').addClass('animation');
                        $('#select17').addClass('animation::-webkit-input-placeholder');
                        $('#select17').focus();
                    } else {
                        c = true;
                    }
                    if ($('#select20').val() == '请使用公司邮箱接收简历' || $('#select20').val() == '')
                    {
                        if ($('#select20').val() == '')
                        {
                            $('#select20').attr('placeholder', '请使用公司邮箱接收简历');
                        }
                        $('#select20').addClass('animation');
                        $('#select20').addClass('animation::-webkit-input-placeholder');
                        $('#select20').focus();
                    }
                    else
                    {
                        if (!$("#select20").val().match(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/)) {
                            $('.email-error').remove();
                            $('#h5').after("<span class='email-error' style=' float: right;color: red;margin-top:18px;'>请使用公司邮箱接收简历</span>");
                            d = false;
                        }
                        else {
                            if ($("#select20").val().match(/^.*@(sina|qq|163|Hotmail).*$/i)) {
                                $('.email-error').remove();
                                $('#h5').after("<span class='email-error' style=' float: right;color: red;margin-top:18px;'>请使用公司邮箱接收简历</span>");
                                d = false;
                            } else {
                                $('.email-error').remove();
                                d = true;
                            }
                        }
                        if ( d ){
                            $("#h5_email_error").hide();
                        }else{
                            $("#h5_email_error").show();
                        }
                    }
                    if ($('#select16').find('span').text() == '请输入职位描述标签')
                    {
                        $('#select16').find('span').remove();
                        $('#select16').append("<span style=' color:red;'>请输入职位描述标签</span>");
                        document.getElementById('select16').scrollIntoView();
                    }
                    else
                    {
                        e = true;
                    }
                    if ($('#select14').find('span').text() == '请输入特别福利标签')
                    {
                        $('#select14').find('span').remove();
                        $('#select14').append("<span style=' color:red;'>请输入特别福利标签</span>");
                        document.getElementById('select14').scrollIntoView();
                    }
                    else
                    {
                        f = true;
                    }
                    if (a == true & b == true & c == true & d == true & e == true & f == true)
                    {
                        $('.tj').click();
                    }

                });

                $(document).on('blur', 'input, textarea', function() {
                    setTimeout(function() {
                        window.scrollTo(document.body.scrollLeft, document.body.scrollTop);
                    }, 0);

                });

                $(".form-group").on("click", function() {
                    $(".black").show();

                });

                $("#formgroup7,#formgroup17,#formgroup17,#formgroup20").off("click");

                $("#formgroup4").on("click", function() {
                    $(".list-group-4").show();
                    $(".list-group-4").css("left", "8%");
                    $("#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select4").css('border-color', '#11beb8');

                });

                $('#group4 > a').each(function() {
                    if ($('#select4').val() == $(this).text())
                    {
                        $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                    } else {
                        $(this).find('img').remove();
                    }
                });
                $('body').on('click', '#group4 > a', function() {
                    $('#select4').val($(this).text());
                    $('#input4').val($(this).attr('value'));
                    $('#group4 > a').each(function() {
                        if ($('#select4').val() == $(this).text())
                        {
                            $(this).find('img').remove();
                            $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                        } else {
                            $(this).find('img').remove();
                        }
                    });
                });

                $("#formgroup5").on("click", function() {
                    $(".list-group-5").show();
                    $(".list-group-5").css("left", "8%");
                    $("#select4,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select5").css('border-color', '#11beb8');

                });
                $('#group5 > a').on('click', function() {
                    var id = $(this).attr('value');
                    $('#input5').val($(this).attr('value'));
                    var group6 = $("#group6");
                    group6.empty();
                    jQuery.ajax({
                        type: 'POST',
                        url: "http://m.morecruit.cn/index.php?s=/addon/Resume/Resume/towIndustry.html",
                        data: "id=" + id,
                        success: function(data)
                        {
                            $.each(data, function(i, n) {
                                if (n.config_default == 1)
                                {
                                    $('#select6').val(n.config_value);
                                    $('#input6').val(n.config_value);
                                }
                                group6.append("<a href='#' value='" + n.config_value + "' class='sel-item list-group-item-6'><span>" + n.config_value + "</span></a>");
                            });
                        }
                    });
                    $('#select5').val($(this).text());
                    $('#group5 > a').each(function() {
                        if ($('#select5').val() == $(this).text())
                        {
                            $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                        } else {
                            $(this).find('img').remove();
                        }
                    });
                });
                $('#group5 > a').each(function() {
                    if ($('#select5').val() == $(this).text())
                    {
                        $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                    } else {
                        $(this).find('img').remove();
                    }
                });
                $('#group5 > a').on('click', function() {
                    $('#select5').val($(this).text());
                    $('#input5').val($(this).attr('value'));
                    $('#group5 > a').each(function() {
                        if ($('#select5').val() == $(this).text())
                        {
                            $(this).find('img').remove();
                            $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                        } else {
                            $(this).find('img').remove();
                        }
                    });
                });

                $("#formgroup6").on("click", function() {
                    $(".list-group-6").show();
                    $(".list-group-6").css("left", "8%");
                    $("#select4,#select5,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select6").css('border-color', '#11beb8');

                });
                $('#group6 > a').each(function() {
                    if ($('#select6').val() == $(this).text())
                    {
                        $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                    } else {
                        $(this).find('img').remove();
                    }
                });
                $('body').on('click', "#group6 > a", function() {
                    $('#select6').val($(this).text());
                    $('#input6').val($(this).attr('value'));
                    $('#group6 > a').each(function() {
                        if ($('#select6').val() == $(this).text())
                        {
                            $(this).find('img').remove();
                            $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                        } else {
                            $(this).find('img').remove();
                        }
                    });
                });

                $("#formgroup7").on("click", function() {
                    $("#select4,#select5,#select6,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select7").css('border-color', '#11beb8');

                });
                $('div').keyup(function() {
                    $('#input7').val($("#select7").val());
                });

                $("#formgroup8").on("click", function() {
                    $(".list-group-8").show("");
                    $(".list-group-8").css("left", "8%");
                    $("#select4,#select5,#select6,#select7,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select8").css('border-color', '#11beb8');

                });
                $('#group8 > a').on('click', function() {
                    var id = $(this).attr('value');
                    $('#input8').val($(this).attr('value'));
                    var group9 = $("#group9");
                    group9.empty();
                    jQuery.ajax({
                        type: 'POST',
                        url: "http://m.morecruit.cn/index.php?s=/addon/Resume/Resume/area.html",
                        data: "id=" + id,
                        success: function(data)
                        {
                            $.each(data, function(i, n) {
                                if (n.config_default == 1)
                                {
                                    $('#select9').val(n.config_value);
                                    $('#input9').val(n.config_value);
                                }
                                group9.append("<a href='#' value='" + n.config_value + "' class='sel-item list-group-item-9'><span>" + n.config_value + "</span></a>");
                            });
                        }
                    });
                    $('#select8').val($(this).text());
                    $('#group8 > a').each(function() {
                        if ($('#select8').val() == $(this).text())
                        {
                            $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                        } else {
                            $(this).find('img').remove();
                        }
                    });
                });
                $('#group8 > a').each(function() {
                    if ($('#select8').val() == $(this).text())
                    {
                        $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                    } else {
                        $(this).find('img').remove();
                    }
                });
                $('#group8 > a').on('click', function() {
                    $('#select8').val($(this).text());
                    $('#input8').val($(this).attr('value'));
                    $('#group8 > a').each(function() {
                        if ($('#select8').val() == $(this).text())
                        {
                            $(this).find('img').remove();
                            $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                        } else {
                            $(this).find('img').remove();
                        }
                    });
                });

                $("#formgroup9").on("click", function() {
                    $(".list-group-9").show("");
                    $(".list-group-9").css("left", "8%");
                    $("#select4,#select5,#select6,#select7,#select8,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select9").css('border-color', '#11beb8');

                });
                $('#group9 > a').each(function() {
                    if ($('#select9').val() == $(this).text())
                    {
                        $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                    } else {
                        $(this).find('img').remove();
                    }
                });
                $('body').on('click', "#group9 > a", function() {
                    $('#select9').val($(this).text());
                    $('#input9').val($(this).attr('value'));
                    $('#group9 > a').each(function() {
                        if ($('#select9').val() == $(this).text())
                        {
                            $(this).find('img').remove();
                            $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                        } else {
                            $(this).find('img').remove();
                        }
                    });
                });

                $("#formgroup10").on("click", function() {
                    $(".list-group-10").show("");
                    $(".list-group-10").css("left", "8%");
                    $(window).off("scroll");
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select10").css('border-color', '#11beb8');

                });

                var a = '';
                $('.tagInfo').empty();
                $('#select16 > button').each(function() {
                    if ($(this).text() != '')
                    {
                        if (a == '')
                        {
                            a = $(this).text();
                        } else {
                            a = a + ',' + $(this).text();
                        }
                        $('.tagInfo').append("<div class='btn-group'><button type='button' class='btn btn-default'>" + $(this).text() + "<span class='glyphicon glyphicon-remove'></span></button></div>");
                    }
                });
                $('#select16 > button').each(function() {
                    $("#tagInfo div:not(.next-title)").each(function() {
                    });
                });
                $('.job_description').val(a);

                $('#select16').on('click', function() {
                    var a = '';
                    $('#select16 > button').each(function() {
                        if ($(this).text() != '')
                        {
                            if (a == '')
                            {
                                a = $(this).text();
                            } else {
                                a = a + ',' + $(this).text();
                            }
                        }
                    });
                    $('.job_description').val(a);
                });
                $('body').on('click', ".tagInfo > div", function() {
                    $(this).remove();
                    $('#tagInfo').append("<div class='btn-group animated bounceIn'><button type='button' class='btn btn-default'>" + $(this).text() + "<span class='glyphicon glyphicon-plus'></span></button></div>");
                    var sc = $(this).text();
                    $('#select16 > button').each(function() {
                        if ($(this).text() == sc)
                        {
                            $(this).remove();
                        }
                    });
                    if ($('.tagInfo').children().length == 0)
                    {
                        $('#select16').append("<span style=' color: #bbbbbb;'>请输入职位描述标签</span>");
                    }
                    var a = ''
                    $('#select16 > button').each(function() {
                        if ($(this).text() != '')
                        {
                            if (a == '')
                            {
                                a = $(this).text();
                            } else {
                                a = a + ',' + $(this).text();
                            }
                        }
                    });
                    $('.job_description').val(a);
                });
                $('body').on('click', "#tagInfo > div:not(.next-title)", function() {
                    if ($('.tagInfo').children().length <= 7)
                    {
                        $(this).remove();
                        $('.tagInfo').append("<div class='btn-group animated fadeInUp'><button type='button' class='btn btn-default'>" + $(this).text() + "<span class='glyphicon glyphicon-remove'></span></button></div>");
                        $('#select16').find('span').remove();
                        $('#select16').append("<button type='button' class='btn btn-default'>" + $(this).text() + "</button>");
                    }
                    var sc = $(this).text();
                    var a = '';
                    $('#select16 > button').each(function() {
                        if ($(this).text() != '')
                        {
                            if (a == '')
                            {
                                a = $(this).text();
                            } else {
                                a = a + ',' + $(this).text();
                            }
                        }
                    });
                    $('.job_description').val(a);
                });
                $('.btn1').on('click', function() {
                    $('#select16').find('span').remove();
                    if ($.trim($('.add').val()) != '')
                    {
                        var adf = 'yes';
                        $('.tagInfo').children().each(function() {

                            if ($(this).text() == $('.add').val())
                            {
                                adf = 'no';
                            }
                        });
                        if ($('.tagInfo').children().length < 8 && adf != 'no')
                        {
                            $('#select16').append("<button type='button' class='btn btn-default'>" + $('.add').val() + "</button>");
                            $('.tagInfo').append("<div class='btn-group animated fadeInUp'><button type='button' class='btn btn-default'>" + $('.add').val() + "<span class='glyphicon glyphicon-remove'></span></button></div>");
                        }

                        $('.add').val('');
                    }
                    var a = '';
                    $('#select16 > button').each(function() {
                        if ($(this).text() != '')
                        {
                            if (a == '')
                            {
                                a = $(this).text();
                            } else {
                                a = a + ',' + $(this).text();
                            }
                        }
                    });
                    $('.job_description').val(a);
                });

                $("#formgroup11").on("click", function() {
                    $(".list-group-11").show("");
                    $(".list-group-11").css("left", "8%");
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select11").css('border-color', '#11beb8');

                });
                $('#group11 > a').each(function() {
                    if ($('#select11').val() == $(this).text())
                    {
                        $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                    } else {
                        $(this).find('img').remove();
                    }
                });
                $('#group11 > a').on('click', function() {
                    $('#select11').val($(this).text());
                    $('#input11').val($(this).attr('value'));

                    $('#group11 > a').each(function() {
                        // var s = $('#select11').val();
                        // var reg = "//s/g";
                        // var ss = s.replace(reg, "");
                        // var c = $(this).text();
                        // var reg = "//s/g";
                        // var cc = c.replace(reg, "");
                        // var test1 = ss;
                        // var test2 = cc;
                        // console.log(test1);

                        if ($('#select11').val() == $(this).text())
                        {
                            console.log($(this).text());
                            $(this).find('img').remove();
                            $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                        } else {
                            $(this).find('img').remove();
                            console.log($(this).text());
                        }
                    });
                });

                $("#formgroup12").on("click", function() {
                    $(".list-group-12").show("");
                    $(".list-group-12").css("left", "8%");
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select12").css('border-color', '#11beb8');

                });
                $('#group12 > a').each(function() {
                    if ($('#select12').val() == $(this).text())
                    {
                        $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                    } else {
                        $(this).find('img').remove();
                    }
                });
                $('#group12 > a').on('click', function() {
                    $('#select12').val($(this).text());
                    $('#input12').val($(this).attr('value'));
                    $('#group12 > a').each(function() {
                        if ($('#select12').val() == $(this).text())
                        {
                            $(this).find('img').remove();
                            $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                        } else {
                            $(this).find('img').remove();
                        }
                    });
                });

                $("#formgroup13").on("click", function() {
                    $(".list-group-13").show("");
                    $(".list-group-13").css("left", "8%");
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select13").css('border-color', '#11beb8');

                });
                $('#group13 > a').each(function() {
                    if ($('#select13').val() == $(this).text())
                    {
                        $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                    } else {
                        $(this).find('img').remove();
                    }
                });
                $('#group13 > a').on('click', function() {
                    $('#select13').val($(this).text());
                    $('#input13').val($(this).attr('value'));
                    $('#group13 > a').each(function() {
                        if ($('#select13').val() == $(this).text())
                        {
                            $(this).find('img').remove();
                            $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                        } else {
                            $(this).find('img').remove();
                        }
                    });
                });

                // $("#formgroup14").on("click", function() {
                //     $(".list-group-14").show("");
                //     $(".list-group-14").css("left", "8%");
                //     $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                //     $("#select14").css('border-color', '#11beb8');

                // });
                // var a = '';
                // $('.tagInfo1').empty();
                // $('#select14 > button').each(function() {
                //     if ($(this).text() != '')
                //     {
                //         if (a == '')
                //         {
                //             a = $(this).text();
                //         } else {
                //             a = a + ',' + $(this).text();
                //         }
                //         $('.tagInfo1').append("<div class='btn-group'><button type='button' class='btn btn-default'>" + $(this).text() + "<span class='glyphicon glyphicon-remove'></span></button></div>");
                //     }
                // });
                // $('.why').val(a);
                // $('#select14').on('click', function() {
                //     var a = '';
                //     $('.tagInfo1').empty();
                //     $('#select14 > button').each(function() {
                //         if ($(this).text() != '')
                //         {
                //             if (a == '')
                //             {
                //                 a = $(this).text();
                //             } else {
                //                 a = a + ',' + $(this).text();
                //             }
                //             $('.tagInfo1').append("<div class='btn-group'><button type='button' class='btn btn-default'>" + $(this).text() + "<span class='glyphicon glyphicon-remove'></span></button></div>");
                //         }
                //     });
                //     $('.why').val(a);
                // });
                // $('body').on('click', '.tagInfo1 > div', function() {
                //     $(this).remove();
                //     $('#beniInfo').append("<div class='btn-group animated bounceIn'><button type='button' class='btn btn-default'>" + $(this).text() + "<span class='glyphicon glyphicon-plus'></span></button></div>");
                //     var sc = $(this).text();
                //     $('#select14 > button').each(function() {
                //         if ($(this).text() == sc)
                //         {
                //             $(this).remove();
                //         }
                //     });
                //     if ($('.tagInfo1').children().length == 0)
                //     {
                //         $('#select14').append("<span style=' color: #bbbbbb;'>请输入特别福利标签</span>");
                //     }
                //     var a = '';
                //     $('#select14 > button').each(function() {
                //         if ($(this).text() != '')
                //         {
                //             if (a == '')
                //             {
                //                 a = $(this).text();
                //             } else {
                //                 a = a + ',' + $(this).text();
                //             }
                //         }
                //     });
                //     $('.why').val(a);
                // });
                // $('body').on('click', "#beniInfo > div:not(.next-title)", function() {
                //     if ($('.tagInfo1').children().length <= 7)
                //     {
                //         $(this).remove();
                //         $('.tagInfo1').append("<div class='btn-group animated fadeInUp'><button type='button' class='btn btn-default'>" + $(this).text() + "<span class='glyphicon glyphicon-remove'></span></button></div>");
                //         $('#select14').find('span').remove();
                //         $('#select14').append("<button type='button' class='btn btn-default'>" + $(this).text() + "</button>");
                //     }
                //     var sc = $(this).text();
                //     var a = '';
                //     $('#select14 > button').each(function() {
                //         if ($(this).text() != '')
                //         {
                //             if (a == '')
                //             {
                //                 a = $(this).text();
                //             } else {
                //                 a = a + ',' + $(this).text();
                //             }
                //         }
                //     });
                //     $('.why').val(a);
                // });
                // $('.btn2').on('click', function() {
                //     $('#select14').find('span').remove();
                //     if ($.trim($('.add1').val()) != '')
                //     {
                //         var adf = 'yes';
                //         $('.tagInfo1').children().each(function() {

                //             if ($(this).text() == $('.add1').val())
                //             {
                //                 adf = 'no';
                //             }
                //         });
                //         if ($('.tagInfo1').children().length < 8 && adf != 'no')
                //         {
                //             $('#select14').append("<button type='button' class='btn btn-default'>" + $('.add1').val() + "</button>");
                //             $('.tagInfo1').append("<div class='btn-group animated fadeInUp'><button type='button' class='btn btn-default'>" + $('.add1').val() + "<span class='glyphicon glyphicon-remove'></span></button></div>");
                //         }

                //         $('.add1').val('');
                //     }
                //     var a = '';
                //     $('#select14 > button').each(function() {
                //         if ($(this).text() != '')
                //         {
                //             if (a == '')
                //             {
                //                 a = $(this).text();
                //             } else {
                //                 a = a + ',' + $(this).text();
                //             }
                //         }
                //     });
                //     $('.why').val(a);
                // });
                $("#formgroup14").on("click", function() {
                    $(".list-group-14").show("");
                    $(".list-group-14").css("left", "8%");
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select14").css('border-color', '#11beb8');

                });
                $('#group14 > a').each(function() {
                    if ($('#select14').val() == $(this).text())
                    {
                        $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                    } else {
                        $(this).find('img').remove();
                    }
                });
                $('#group14 > a').on('click', function() {
                    $('#select14').val($(this).text());
                    $('#input14').val($(this).attr('value'));
                    $('#group14 > a').each(function() {
                        if ($('#select14').val() == $(this).text())
                        {
                            $(this).find('img').remove();
                            $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                        } else {
                            $(this).find('img').remove();
                        }
                    });
                });


                $("#formgroup15").on("click", function() {
                    $(".list-group-15").show("");
                    $(".list-group-15").css("left", "8%");
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select15").css('border-color', '#11beb8');

                });
                $('#group15 > a').each(function() {
                    if ($('#select15').val() == $(this).text())
                    {
                        $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                    } else {
                        $(this).find('img').remove();
                    }
                });
                $('#group15 > a').on('click', function() {
                    $('#select15').val($(this).text());
                    $('#input15').val($(this).attr('value'));
                    $('#group15 > a').each(function() {
                        if ($('#select15').val() == $(this).text())
                        {
                            $(this).find('img').remove();
                            $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                        } else {
                            $(this).find('img').remove();
                        }
                    });
                });

                // $("#formgroup16").on("click", function() {
                //     $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                //     $("#select16").css('border-color', '#11beb8');

                // });
                // $('div').keyup(function() {
                //     $('#input16').val($("#select16").val());
                // });
                $("#formgroup16").on("click", function() {
                    $(".list-group-16").show("");
                    $(".list-group-16").css("left", "8%");
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select16").css('border-color', '#11beb8');

                });
                $('#group16 > a').each(function() {
                    if ($('#select16').val() == $(this).text())
                    {
                        $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                    } else {
                        $(this).find('img').remove();
                    }
                });
                $('#group16 > a').on('click', function() {
                    $('#select16').val($(this).text());
                    $('#input16').val($(this).attr('value'));
                    $('#group16 > a').each(function() {
                        if ($('#select16').val() == $(this).text())
                        {
                            $(this).find('img').remove();
                            $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                        } else {
                            $(this).find('img').remove();
                        }
                    });
                });

                $("#formgroup17").on("click", function() {
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select17").css('border-color', '#11beb8');

                });
                $('div').keyup(function() {
                    $('#input17').val($("#select17").val());
                });

                $("#formgroup18").on("click", function() {
                    $(".list-group-18").show("");
                    $(".list-group-18").css("left", "8%");
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select18").css('border-color', '#11beb8');

                });
                $('#group18 > a').each(function() {
                    if ($('#select18').val() == $(this).text())
                    {
                        $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                    } else {
                        $(this).find('img').remove();
                    }
                });
                $('#group18 > a').on('click', function() {
                    $('#select18').val($(this).text());
                    $('#input18').val($(this).attr('value'));
                    $('#group18 > a').each(function() {
                        if ($('#select18').val() == $(this).text())
                        {
                            $(this).find('img').remove();
                            $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                        } else {
                            $(this).find('img').remove();
                        }
                    });
                });

                $("#formgroup19").on("click", function() {
                    $(".list-group-19").show("");
                    $(".list-group-19").css("left", "8%");
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select20").css('border-color', '#e5e5e5');
                    $("#select19").css('border-color', '#11beb8');

                });
                $('#group19 > a').each(function() {
                    if ($('#select19').val() == $(this).text())
                    {
                        $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                    } else {
                        $(this).find('img').remove();
                    }
                });
                $('#group19 > a').on('click', function() {
                    $('#select19').val($(this).text());
                    $('#input19').val($(this).attr('value'));
                    $('#group19 > a').each(function() {
                        if ($('#select19').val() == $(this).text())
                        {
                            $(this).find('img').remove();
                            $(this).find('span').append('<img src="/Public/img/moicon-selected.png">');
                        } else {
                            $(this).find('img').remove();
                        }
                    });
                });

                $("#formgroup20").on("click", function() {
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19").css('border-color', '#e5e5e5');
                    $("#select20").css('border-color', '#11beb8');

                });
                $('div').keyup(function() {
                    $('.email').val($("#select20").val());
                });

                $(".list-group-item-4").on("click", function() {
                    $(".list-group-4").animate({left: "-100%"}, 500);
                    $(".list-group-4").hide("");
                    $(".list-group-5").show("");
                    $(".list-group-5").animate({left: "6%"}, 500);
                    $("#select4,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select5").css('border-color', '#11beb8');

                });

                $(".list-group-item-5").on("click", function() {
                    $(".list-group-5").animate({left: "-100%"}, 500);
                    $(".list-group-5").hide("");
                    $(".list-group-6").show("");
                    $(".list-group-6").animate({left: "6%"}, 500);
                    $("#select4,#select5,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select6").css('border-color', '#11beb8');

                });

                $("body").on("click", ".list-group-item-6", function() {
                    $(".black").hide();
                    $(".select").hide();
                    $("#select7").focus();
                    $("#select7").css('border-color', '#11beb8');
                    $("#select4,#select5,#select6,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');

                });

                $("#next7").on("click", function() {
                    $(".list-group-7").animate({left: "-100%"}, 500);
                    $(".list-group-7").hide("");
                    $(".list-group-8").show("");
                    $(".list-group-8").animate({left: "6%"}, 500);
                    $("#select4,#select5,#select6,#select7,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select8").css('border-color', '#11beb8');
                    $("html,body").animate({scrollTop: $("#formgroup6").offset().top}, 1000);

                });

                $(".list-group-item-8").on("click", function() {
                    $(".list-group-8").animate({left: "-100%"}, 500);
                    $(".list-group-8").hide("");
                    $(".list-group-9").show("");
                    $(".list-group-9").animate({left: "6%"}, 500);
                    $("#select4,#select5,#select6,#select7,#select8,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select9").css('border-color', '#11beb8');
                    $("html,body").animate({scrollTop: $("#formgroup7").offset().top}, 1000);

                });

                $("body").on("click", ".list-group-item-9", function() {
                    $(".list-group-9").animate({left: "-100%"}, 500);
                    $(".list-group-9").hide("");
                    $(".list-group-10").show("");
                    $(".list-group-10").animate({left: "6%"}, 500);
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select10").css('border-color', '#11beb8');
                    $("html,body").animate({scrollTop: $("#formgroup8").offset().top}, 1000);

                });

                $("#next10").on("click", function() {
                    $(".list-group-10").animate({left: "-100%"}, 500);
                    $(".list-group-10").hide("");
                    $(".list-group-11").show("");
                    $(".list-group-11").animate({left: "6%"}, 500);
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select11").css('border-color', '#11beb8');
                    $("html,body").animate({scrollTop: $("#formgroup9").offset().top}, 1000);

                });

                $(".list-group-item-11").on("click", function() {
                    $(".list-group-11").animate({left: "-100%"}, 500);
                    $(".list-group-11").hide("");
                    $(".list-group-12").show("");
                    $(".list-group-12").animate({left: "6%"}, 500);
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select13,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select12").css('border-color', '#11beb8');
                    $("html,body").animate({scrollTop: $("#formgroup10").offset().top}, 1000);

                });

                $(".list-group-item-12").on("click", function() {
                    $(".list-group-12").animate({left: "-100%"}, 1000);
                    $(".list-group-12").hide("");
                    $(".list-group-13").show("");
                    $(".list-group-13").animate({left: "6%"}, 500);
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select14,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select13").css('border-color', '#11beb8');
                    $("html,body").animate({scrollTop: $("#formgroup11").offset().top}, 1000);

                });

                $(".list-group-item-13").on("click", function() {
                    $(".list-group-13").animate({left: "-100%"}, 500);
                    $(".list-group-13").hide("");
                    $(".list-group-14").show("");
                    $(".list-group-14").animate({left: "6%"}, 500);
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select15,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select14").css('border-color', '#11beb8');
                    $("html,body").animate({scrollTop: $("#formgroup12").offset().top}, 1000);

                });

                $(".list-group-item-14").on("click", function() {
                    $(".list-group-14").animate({left: "-100%"}, 500);
                    $(".list-group-14").hide("");
                    $(".list-group-15").show("");
                    $(".list-group-15").animate({left: "6%"}, 500);
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select16,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select15").css('border-color', '#11beb8');
                    $("html,body").animate({scrollTop: $("#formgroup13").offset().top}, 1000);

                });
                 $(".list-group-item-15").on("click", function() {
                    $(".list-group-15").animate({left: "-100%"}, 500);
                    $(".list-group-15").hide("");
                    $(".list-group-16").show("");
                    $(".list-group-16").animate({left: "6%"}, 500);
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select16").css('border-color', '#11beb8');
                    $("html,body").animate({scrollTop: $("#formgroup14").offset().top}, 1000);

                });

                // $(".list-group-item-15").on("click", function() {
                //     $(".black").hide();
                //     $(".select").hide();
                //     $(".list-group-15").hide("");
                //     $(".list-group-16").show("");
                //     $(".list-group-16").animate({left: "6%"}, 500);
                //     $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                //     // $("#select16").focus();
                //     $("#select16").css('border-color', '#11beb8');
                //     $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select17,#select18,#select19,#select20").css('border-color', '#e5e5e5');

                // });

                $(".list-group-item-16").on("click", function() {
                    $(".list-group-16").animate({left: "-100%"}, 500);
                    $(".list-group-16").hide("");
                    $(".list-group-17").show("");
                    $(".list-group-17").animate({left: "6%"}, 500);
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select18,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select17").css('border-color', '#11beb8');
                    $("html,body").animate({scrollTop: $("#formgroup15").offset().top}, 1000);

                });

                $("#next17").on("click", function() {
                    $(".list-group-17").animate({left: "-100%"}, 500);
                    $(".list-group-17").hide("");
                    $(".list-group-18").show("");
                    $(".list-group-18").animate({left: "6%"}, 500);
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select19,#select20").css('border-color', '#e5e5e5');
                    $("#select18").css('border-color', '#11beb8');
                    $("html,body").animate({scrollTop: $("#formgroup16").offset().top}, 1000);

                });

                $(".list-group-item-18").on("click", function() {
                    $(".list-group-18").animate({left: "-100%"}, 500);
                    $(".list-group-18").hide("");
                    $(".list-group-19").show("");
                    $(".list-group-19").animate({left: "6%"}, 500);
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select20").css('border-color', '#e5e5e5');
                    $("#select19").css('border-color', '#11beb8');
                    $("html,body").animate({scrollTop: $("#formgroup17").offset().top}, 1000);

                });

                $(".list-group-item-19").on("click", function() {
                    $(".black").hide();
                    $(".select").hide();
                    $("#select20").focus();
                    $("#select20").css('border-color', '#11beb8');
                    $("#select4,#select5,#select6,#select7,#select8,#select9,#select10,#select11,#select12,#select13,#select14,#select15,#select16,#select17,#select18,#select19").css('border-color', '#e5e5e5');
                    $("#select20").css('border-color', '#11beb8');

                });

                $("#backGround,#selClose,.last").on("click",function(){
                    $(".select").hide();
                    $("#black").hide();
                    $(".select").css("left","100%");
                });

                $('#exit_web_publish_job').on('click', function () {
                    $('#div_exit_web_publish_job').hide();
                });


            });

            function refreshValidateEmailStatus(email)
            {
                var st = setInterval(function () {
                    $._getJSONX('/index.php?s=/addon/Resume/Publish/getEmailStatus', {
                        "email" : email
                    }, function (resp) {
                        if (resp.msg) {
                            clearInterval(st);
                            $('#select20').prop('readonly', true);
                            $('#select20').addClass('checked');
                            $('#div_check_email_msg').hide();
                        }
                    });
                }, 5000);
            }

            function checkEmail(email)
            {
                if (email == '请使用公司邮箱接收简历' || email == '') {
                    $.mbox.alert('邮箱不能为空');
                    return false;
                }

                if (! email.match(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/)) {
                    $.mbox.alert('邮箱格式错误');
                    return false;
                }

                if (email.match(/^.*@(sina|qq|163|Hotmail).*$/i)) {
                    $.mbox.alert('请使用公司邮箱接收简历');
                    return false;
                }

                return true;
            }